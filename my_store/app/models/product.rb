class Product < ActiveRecord::Base
  attr_accessible :name, 
                  :description,
                  :price,
                  :weight,
                  :user_id,
                  :category_id

  belongs_to :user
  belongs_to :category
end
