class CommentsController < ApplicationController
  before_filter :require_admin_login, :only=>[:create]
  def create
      @comment=Comment.new(params[:comment])
      article=Article.find(params[:comment][:article_id].to_i)
      respond_to do |format|
         @comment.save
         format.html { redirect_to(article_path(article), :notice => 'Comment was successfully created.') }
         format.js { @comments = article.comments }
      end
   end
end
