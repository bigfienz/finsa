class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user

  def require_login
    if current_user.nil?
      flash[:error] ="Only admins are permited"
      redirect_to sign_in_path
    else
      return current_user
    end
  end

  private
  def current_user
    @current_user ||=User.find(session[:user_id]) if session[:user_id]
  end

end
