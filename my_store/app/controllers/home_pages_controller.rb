class HomePagesController < ApplicationController
  def index
    @articles=Article.find_by_sql("SELECT * FROM `articles` ORDER BY created_at DESC LIMIT 3")
    @products=Product.find_by_sql("SELECT * FROM `products` ORDER BY created_at DESC LIMIT 6")
  end
end
