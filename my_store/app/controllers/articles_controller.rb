class ArticlesController < ApplicationController
  before_filter :require_admin_login, :only=>[:new,:create]
  def index
    @articles=Article.all
  end
  
  def new
    @article=Article.new
  end

  def create
    @article=Article.new(params[:article])
    @article.user_id=session[:user_id]
    if @article.save
      redirect_to articles_path
    else
      flash[:error]="cannot create new article"
      render "new"
    end
  end

  def show
    @article=Article.find(params[:id])
    @comments=@article.comments
    @comment=Comment.new
  end
  
end
