class UsersController < ApplicationController
  before_filter :find_by_id, :only => [:show,:edit,:update]
  
  def new
    @user=User.new
  end

  def create
    @user=User.new(params[:user])
    if verify_recaptcha
      if @user.save    
        UserMailer.registration_confirmation(@user).deliver
        session[:user_id] = @user.id
        session[:user_name] = @user.name
        flash[:notice] = 'Signed Up!'
        redirect_to user_path(session[:user_id])
      else
        render "new"
      end
    else
      flash[:error] = "There was an error with the recaptcha code below.
                       Please re-enter the code and click submit. "
      render "new"
    end
  end

  def show
    
  end
  
  def edit

  end

  def update
    if @user.update_attributes(params[:user])
      flash[:notice]= 'User has ? has been updated', @user.email
      redirect_to user_path(session[:user_id])
    else
      flash[:error]= 'Cannot update user ?', @user.email
      render "edit"
    end
  end

  private
  def find_by_id
    @user=User.find(params[:id])
  end

end
