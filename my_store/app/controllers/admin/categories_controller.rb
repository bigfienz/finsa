class Admin::CategoriesController < Admin::ApplicationController
  before_filter :require_admin_login
  before_filter :find_by_id,
                :only => [:show]

  def index
    @categories=Category.all
  end
  def new
    @parent_categories = Category.where(["parent_id IS NULL"]).map{|x| [x.name, x.id]}
    @category=Category.new
  end

  def create
    @category=Category.new(params[:category])
    if @category.save
      flash[:notice]="New Category has been created"
      redirect_to admin_categories_path
    else
      flash[:error]="Create new category fail"
      render "new"
    end
  end
  
  def show
    
  end

  private
  def find_by_id
    @category=Category.find(params[:id])
  end
end
