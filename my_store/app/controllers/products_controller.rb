class ProductsController < ApplicationController
  before_filter :require_login, :only=>[:new,:create]
  before_filter :find_by_id, :only=>[:edit,:update, :destroy]

  def index
    @products=Product.all
  end

  def new
    @product=Product.new
    @categories=Category.all.map{|x| [x.name, x.id]}
  end
  
  def create
    @product=Product.new(params[:product])
    @product.user_id=session[:user_id]
    if @product.save
      flash[:notice]='Product has been created'
      redirect_to products_path
    else
      flash[:error]='Cannot create new product'
      render "new"
    end
  end
  
  def show
    @product=Product.find(params[:id])
  end
  
  def find_by_category
  @products = Product.find_all_by_category_id(params[:category_id])
  render "index"
  end

  def edit
    @categories=Category.all.map{|x| [x.name, x.id]}
  end

  def update
    if @product.user_id == session[:user_id] || User.find(session[:user_id]).email == "admin@admin.com"
      if @product.update_attributes(params[:product])
        redirect_to products_path
      else
        flash[:error]='Cannot update product'
        render "edit"
      end
    else
      flash[:error]="You not permitted to update product"
      redirect_to products_path
    end
  end

  def destroy
    if @product.user_id == session[:user_id] || User.find(session[:user_id]).email == "admin@admin.com"
      if @product.destroy
        redirect_to products_path
      else
        flash[:error]='Cannot delete product'
        redirect_to products_path
      end
    else
      flash[:error]="You not permitted to delete product"
      redirect_to products_path
    end
  end

  private
  def find_by_id
    @product=Product.find(params[:id])
  end
end
