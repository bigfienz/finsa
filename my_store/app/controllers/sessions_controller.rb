class SessionsController < ApplicationController
  def new
  end
  
  def create
    user = User.authenticate(params[:email],params[:password])
    if user
      if !user.admin?
        session[:user_id] = user.id
        session[:user_name] = user.name
        flash[:notice] = 'Logged in!'
        redirect_to user_path(session[:user_id])
      else
        session[:user_id] = user.id
        session[:user_name] = user.name
        flash[:notice] = 'Logged in as Admin!'
        redirect_to user_path(session[:user_id])
      end
    else
      flash[:error] = "Invalid email or password"
      render "new"
    end
  end
  
  def destroy
    session[:user_id] =nil
    flash[:notice] = 'Logged out! '
    redirect_to root_path
  end
end
