require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @user = User.find(:first)
  end

  def test_new
    get :new
    assert_nil assigns(:post)
    assert_response :success
  end

  def test_create
    assert_difference('User.count') do
      post :create, :user => {:email => 'ruby@gmail.com', :password => '123', :password_confirmation => '123'}
      assert_not_nil assigns(:user)
      assert_equal assigns(:user).email, "ruby@gmail.com"
    end
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], 'Signed up!'
  end

  def test_create_with_invalid_parameter
    assert_no_difference('User.count') do
      post :create, :user => {:email=>nil, :password => nil, :password_confirmation =>nil}
      assert_not_nil assigns(:user)
      assert_equal assigns(:user).valid?, false
    end
    assert_response :success
  end

end
