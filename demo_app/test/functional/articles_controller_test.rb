require 'test_helper'
class ArticlesControllerTest < ActionController::TestCase
  def setup
    @article=Article.find(:first)

  end

  def test_index
    login_as('bigfienz@gmail.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end
  
  def test_new
    login_as('bigfienz@gmail.com')
    get :new
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_create
    login_as('bigfienz@gmail.com')
    assert_difference('Article.count') do
      post :create, :article =>{:id => 1, :title =>'unit test', :body =>'unit testing', :rating => 8, :user_id => 4}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).title, "unit test"
      assert_equal assigns(:article).valid?, true
    end
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], 'Article was successfully created.'
  end
  
    def test_create_with_invalid_data
    login_as('bigfienz@gmail.com')
    assert_no_difference('Article.count') do
      post :create, :article =>{:id => 1, :title =>nil, :body =>nil, :rating => nil, :user_id => nil}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).valid?, false
    end
    assert_response :success
    assert_equal flash[:error], 'Article was failed to create.'
  end

  def test_edit
    login_as('bigfienz@gmail.com')
    get :edit, :id =>Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end
  
  def test_update
    login_as('bigfienz@gmail.com')
    put :update, :id => Article.first.id, :article => {:title => 'update title', :body => 'update body'}
    assert_not_nil assigns(:article)
    assert_equal assigns(:article).title, "update title"
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], 'Article was successfully updated.'
  end

  def test_show
    login_as('bigfienz@gmail.com')
    get :show, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_destroy
    login_as('bigfienz@gmail.com')
    assert_difference('Article.count', -1) do
      delete :destroy, :id=> Article.first.id
    end
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice],'Article was successfully deleted.' 
  end
end
