require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  def test_save_without_title
    article=Article.new(:body =>"unit testing", :rating => 8, :user_id => 4)
    assert_equal article.valid?, false
    assert_equal article.save, false
  end
  
  def test_save
    article=Article.new(:title =>"unit test", :body =>"unit testing", :rating => 8, :user_id => 4)
    assert_equal article.valid?, true
    assert_equal article.save, true
  end
end
