require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def test_save_without_email
    user=User.new(:password => '123', :password_confirmation => '123')
    assert_equal user.valid?, false
    assert_equal user.save, false
  end

  def test_password_are_equal_with_confirm_password
    user=User.new(:email => "test@gmail.com",:password => '1234', :password_confirmation => '1235')
    assert_not_equal user.password, user.password_confirmation
    assert_equal user.save, false
  end

end
