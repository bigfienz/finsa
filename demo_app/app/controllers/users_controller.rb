class UsersController < ApplicationController
  def new
    @user = User.new
  end
  def create
    @user = User.new(params[:user])
    if verify_recaptcha
       if @user.save
         UserMailer.registration_confirmation(@user).deliver
         session[:user_id] = @user.id
         flash[:notice] = 'Signed up!'
         redirect_to :controller => :articles, :action => :index
       else
         render "new"
       end
    else
       flash[:error] = "There was an error with the rechaptcha code below.
                        Please re-enter the code and click submit. "
       render "new"
    end
  end

end
