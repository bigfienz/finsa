class ArticlesController < ApplicationController
   before_filter :require_login   
   before_filter :find_article, :only => [:edit, :update, :show, :destroy]
   
   #, :only => [:new, :create, :edit, :update, :destroy]
   
   #show the article list
   def index
      @articles=Article.find_all_by_user_id(current_user.id)
   end 

   #create new article
   def new 
      @article=Article.new
   end
 
   def create
      @article=Article.new(params[:article])
      @article.user_id = session[:user_id]
      if @article.save
         flash[:notice]='Article was successfully created.'
         redirect_to articles_path
      else
         flash[:error] = 'Article was failed to create.'
         render :action => "new"
      end
   end

   #update article
   def edit 
   end

   def update
      if @article.update_attributes(params[:article])
         flash[:notice]='Article was successfully updated.'
         redirect_to articles_path
      else
         flash[:error] = 'Article was failed to update.'
         render :action => "edit"
      end
   end

   #delete article
   def destroy
      if @article.destroy
         flash[:notice]='Article was successfully deleted.'
         redirect_to articles_path
      else
         flash[:error] = 'Article was failed to delete.'
         render :action => "index"
      end
   end  
   
   def show
     @comments=@article.comments
     @comment=Comment.new
   end
   
   private
   def find_article
      @article=Article.find(params[:id])
   end
end

