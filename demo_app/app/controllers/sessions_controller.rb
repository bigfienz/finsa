class SessionsController < ApplicationController
  
  def new
  end
  
  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      if !user.is_admin?
         session[:user_id] = user.id
         flash[:notice] = 'Logged in!'
         redirect_to :controller => :articles, :action => :index
      else
         session[:user_id] = user.id
         flash[:notice] = 'Logged in as Admin!'
         redirect_to admin_articles_path
      end
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice] = 'Logged out!'
    redirect_to :controller => :sessions, :action => :new, :notice => "Logged out!"
  end  
  
end
