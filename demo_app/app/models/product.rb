class Product < ActiveRecord::Base
   attr_accessible :name, :price, :description, :user_id
   has_many :categories, :through => :products_category
   has_many :products_category
   belongs_to :user
   scope :greater_than_1000, where("price > 1000")
   scope :contain_red, where("name like '%red%'")
end
