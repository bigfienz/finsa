class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :email, :username, :address, :age, :birthday, :password, :password_confirmation, :admin
  attr_accessor :password
  before_save :encrypt_password
  validates :password, :presence => {:on => :create},
                          :confirmation => true
  validates :email, :presence => true, 
                          :uniqueness => true  

   #relationship table
   has_many :products, :dependent => :destroy
   has_many :articles, :dependent => :destroy
   has_one :contry

   #instance method
   def full_address
     "#{self.address} #{self.contries.name}"
   end

   def encrypt_password
     if password.present?
        self.password_salt = BCrypt::Engine.generate_salt
        self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
     end
   end
   
   def is_admin?
     if self.admin == 1
        true
     else
        false
     end
   end

   def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end
