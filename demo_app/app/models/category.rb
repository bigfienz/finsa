class Category < ActiveRecord::Base
  attr_accessible :name
   has_many :products, :through => :products_category
   has_many :products_category
end
