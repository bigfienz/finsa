class Article < ActiveRecord::Base
  attr_accessible :id, :title, :body, :rating, :user_id
   validates :title, :allow_blank =>false,
                     :uniqueness =>true,
                     :presence =>true
   validate :title_validation
   has_many :comments, :dependent => :destroy
   belongs_to :user
   scope :rating, lambda{|rating| where("rating = ?", rating)}

   def get_100_article_content
      self.where("body.count >100")
   end

   def title_validation
     self.errors[:title]<<"body must be present" if self.title == "nil" || self.title == "empty" || self.title == "blank" 
   end
end
