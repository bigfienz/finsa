# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users=User.create([{first_name: 'Finsa', last_name: 'Anugrah Pratama', email: 'finsa.anugrah@kiranatama.com',username: 'bigfienz',address: 'Margahayu Raya', age: 23, birthday: '11st January 1989'},{first_name: 'Muhammad', last_name: 'Fitra Irfansyah', email: 'fitrairfansyah@yahoo.com',username: 'fitrairfansyah',address: 'Margahayu Raya', age: 16, birthday: '17th February 1996'},{first_name: 'Firna', last_name: 'Tiara Putri', email: 'firnatiara@yahoo.com',username: 'firnatiara',address: 'Margahayu Raya', age: 12, birthday: '6st June 2000'},{first_name: 'Ayi', last_name: 'Koswara', email: 'ayi_koswara@gmail.com',username: 'koswara',address: 'Margahayu Raya', age: 51, birthday: '31st October 1961'},{first_name: 'Rosyanti', email: 'soryanti@kiranatama.com',username: 'rosyanti',address: 'Margahayu Raya', age: 49, birthday: '6th September 1963'}])

countries=Contry.create([{code:'bdo',name:'bandung'},{code:'ckg',name:'jakarta'},{code:'bth',name:'batam'},{code:'tkg',name:'tanjung karang'},{code:'sby',name:'surabaya'}])

articles=Article.create([{title:'ruby on rails for dummie',body:'ruby on rails for dummie',rating:10},{title:'html5 for dummie',body:'html5 for dummie',rating:9},{title:'javascript for dummie',body:'javasrcipt for dummie',rating:9},{title:'mongoDb for dummie',body:'mongoDb for dummie',rating:8},{title:'CSS for dummie',body:'CSS for dummie',rating:10}])

comments=Comment.create([{content: 'comment1'},{content: 'comment2'},{content: 'comment3'},{content: 'comment4'},{content: 'comment5'}])
