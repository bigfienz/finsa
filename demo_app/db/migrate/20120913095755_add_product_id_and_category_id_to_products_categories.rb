class AddProductIdAndCategoryIdToProductsCategories < ActiveRecord::Migration
  def up
    add_column :products_categories, :product_id, :integer
    add_column :products_categories, :category_id, :integer
  end
  def down
    remove_column :products_categories, :product_id
    remove_column :products_categories, :category_id
  end
end
