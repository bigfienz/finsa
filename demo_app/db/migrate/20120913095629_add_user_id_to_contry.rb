class AddUserIdToContry < ActiveRecord::Migration
  def up
    add_column :contries, :user_id, :integer
  end
  def down
    remove_column :contries, :user_id
  end
end
