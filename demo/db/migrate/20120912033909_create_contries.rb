class CreateContries < ActiveRecord::Migration
  def change
    create_table :contries do |t|
      t.integer :code
      t.string :name
      t.timestamps
    end
  end
end
