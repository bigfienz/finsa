class Articles < ActiveRecord::Base
   attr_accessible :title, :body, :rating
   validates :title, :allow_blank =>false,
                    :uniqueness =>true
   validate :title_validation
   has_many :comments, :dependent => :destroy
   belongs_to :user
   scope :rating, lambda{|rating| where("rating = ?", rating)}

   def get_100_article_content
      self.where("body.count >100")
   end

   def title_validation
     if self.body = "nil" || self.body = "empty" || self.body = "blank" 
        then result ="can't fill #{self.body}"
     end
   end
end
