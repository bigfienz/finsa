class Users < ActiveRecord::Base
   attr_accessible :first_name, :last_name, :email, :username, :address, :age, :birthday 
      validates :first_name, :length => {:maximum =>20},
                          :uniqueness => true,
                          :allow_nil =>false

   #relationship table
   has_many :products, :dependent => :destroy
   has_many :articles, :dependent => :destroy
   has_one :contry

   #instance method
   def full_address
     "#{self.address} #{self.contries.name}"
   end
   
end
